# imported modules that are used throughout this code
import sys
import argparse
from itertools import groupby 
import re

#Invoke parser command and insert help + output + input data
parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output", help="Directs the output to a name of output file")
parser.add_argument("file", type=argparse.FileType("r"), nargs="+")
args = parser.parse_args()

#Read from parser data
inputArguments = args.file
count = len(inputArguments)
x = 1
finalContents = ""
usernameList = ""
sLetter=""
i = 1
#Read through input files and put data into single variable
while x <= count: 
	with open(sys.argv[x+2], "r") as f:
		contents = f.read()
	finalContents= finalContents +("\n")+str(contents)	
	x+=1

x = 1
y = 0
#Generate usernames
while x <= len(finalContents.split(":"))-1:
	fLetter = finalContents.lower().split(":")[x][0][0]
	if finalContents.lower().split(":")[x+1]:
		sLetter= finalContents.lower().split(":")[x+1][0][0]
		surname = finalContents.lower().split(":")[x+2][:6]
		usernameList = usernameList+str(fLetter+sLetter+surname)+("\n")
	else:
		surname = finalContents.lower().split(":")[x+2][:7]
		usernameList = usernameList+str(fLetter+surname)+("\n")
		
	x+=4
#Check for duplicates. If found, replace them with username+incremented number
duplicates = usernameList.split()
formatted_username = ""
for word in duplicates :
    if word not in formatted_username:   
        formatted_username = formatted_username +' '+word + ("\n")
    else:
        formatted_username = str(formatted_username) +' ' + str(word) + str(i) + ("\n")
        i+=1

#Insert usernames back to previous variable
x = 1
y = 0
formatted_username = formatted_username.split()
addUsername = finalContents.split(":")
while x <= len(finalContents.split(":")):
	addUsername.insert(x,formatted_username[y])
	x+=5
	y+=1
#Add back : symbol after previous cleanup
addedDots =":".join(f"{addUsername[i:i+1]}" for i in range(0, len(addUsername), 1))
newadddots = re.sub("[^\\ :a-zA-Z0-9]","", addedDots)

#Save to Output.txt file
with open(args.output, "w") as output_file:
    output_file.write("%s\n" % newadddots)
